<?php
function wpt_theme_js()
{
    //Lazy
//    wp_register_script('lazy_js', get_template_directory_uri() . '/assets/libs/lazy/jquery.lazy.min.js', array('jquery'));
//    wp_enqueue_script('lazy_js');
    //Slick
    wp_register_script('slick_js', get_template_directory_uri() . '/assets/libs/slick/slick/slick.min.js', array(), null, true);
    wp_enqueue_script('slick_js');
    // wow
    wp_register_script('wow_js', get_template_directory_uri() . '/assets/libs/wow/wow.min.js', array(), null, true);
    wp_enqueue_script('wow_js');
    // App
    wp_register_script('app_js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '0.1', true);
    wp_enqueue_script('app_js');
}

add_action('wp_enqueue_scripts', 'wpt_theme_js');