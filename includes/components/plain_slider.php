<div class="journal_slider wow fadeIn" data-wow-delay="0.3s" data-wow-duration="1.4s">
    <?php while (have_rows('gallery')) : the_row(); ?>
        <?php $img = get_sub_field('image'); ?>
        <img src="<?php echo $img['sizes']['big']; ?>" alt="<?php echo $img['title']; ?>">
    <?php endwhile; ?>
</div>