<?php
//Register cpt
add_action('init', 'create_custom_post_types');
function create_custom_post_types()
{
   register_post_type('Journal',
       array(
           'labels' => array(
               'name' => __('Journal', 'eds'),
               'singular_name' => __('Journal', 'eds')
           ),
           'public' => true,
           'has_archive' => true,
           'menu_icon' => 'dashicons-location-alt',
           'supports' => [
               'title', 'editor', 'thumbnail', 'excerpt'
           ],
           'rewrite' => array('slug' => 'journal', 'with_front' => false)
       )
   );
   register_post_type('Portfolio',
       array(
           'labels' => array(
               'name' => __('Portfolio', 'eds'),
               'singular_name' => __('Portfolio', 'eds')
           ),
           'public' => true,
           'has_archive' => true,
           'menu_icon' => 'dashicons-location-alt',
           'supports' => [
               'title', 'editor', 'thumbnail', 'excerpt'
           ],
           'rewrite' => array('slug' => 'portfolio', 'with_front' => false)
       )
   );
}