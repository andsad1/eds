<section id="instagram_section">
    <h1>Follow Us On Instagram</h1>
    <?php echo do_shortcode('[insta-gallery id="1"]'); ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="bottom_line"></div>
            </div>
        </div>
    </div>
</section>