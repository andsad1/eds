<section id="portf_section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="portf_row">
                    <div class="portf_item wow fadeIn" data-wow-offset="200">
                        <a href="">
                            <div class="portf_background" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/portf1.png)">

                            </div>
                            <h3>
                                Residential Design
                            </h3>
                        </a>
                    </div>
                    <div class="portf_item wow fadeIn" data-wow-delay="0.2s" data-wow-offset="200">
                        <a href="">
                            <div class="portf_background" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/portf2.jpg)">

                            </div>
                            <h3>
                                Commercial Design
                            </h3>
                        </a>
                    </div>
                    <div class="portf_item wow fadeIn" data-wow-delay="0.4s" data-wow-offset="200">
                        <a href="">
                            <div class="portf_background" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/portf3.jpg)">

                            </div>
                            <h3>
                                Furniture & Joinery Design
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>