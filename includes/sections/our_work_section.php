<section id="about_us_section" class="wow fadeIn" data-wow-offset="200">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <h1>Our Work</h1>
                <div class="text">
                    <p>
                        Designing as little or as much as you require within your specified budget, we work with our clients to create exceptional interiors and interior products for both residential and commercial spaces. We collaborate with talented architects, contractors, and craftspeople to bring your vision to life. We offer a complete interior architecture and interior design service from the concept stage, through to on-site implementation. Over the years we have curated an extensive list of suppliers and our love of sourcing distinct and unique pieces gives our projects an eclectic yet timeless quality full of personality. Our approach to every project is unique and is based upon the individual response to the project at hand. We would describe our own style as quite eclectic, however we appreciate all good design whether that happens to be more traditional or very contemporary. We love combining styles and mixing periods to create interiors with a soul that tell a story.
                        Current projects that we are working on include the complete interior design and restoration of a traditional stone farmhouse with a new contemporary barn extension; luxury apartment for holiday letting; partial refurbishment of a 4-star boutique hotel.
                    </p>
                </div>
                <div class="main_button_holder">
                    <a class="main_button wow fadeInDown" href="">
                        Contact Us
                    </a>
                </div>
                <div class="bottom_line wow fadeIn"></div>
            </div>
        </div>
    </div>
</section>