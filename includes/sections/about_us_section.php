<section id="about_us_section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
            <h1 class="wow fadeIn" data-wow-offset="200">Welcome to our Design World</h1>
                <div class="text wow fadeIn" data-wow-offset="200">
                    <div class="small">
                        <p>
                            “To design is much more than simply to assemble, to order, or even to edit: it is to add value and meaning, to illuminate, to simplify, to clarify, to modify, to dignify, to dramatize, to persuade, and perhaps even to amuse…” Paul Rand
                        </p>
                    </div>
                    <p>
                        EDIT is an Interior Design Studio co-founded by Nathalie Vos and Justina Gruzdyte in 2018 in the beautiful town of Kenmare on the Wild Atlantic Way.
                        With an international portfolio of projects both residential and commercial spanning the last 14 years, our aim is to design and curate beautiful and considered spaces that reflect our clients’ personalities and visions of aesthetics and functionality. Our philosophy is simple: Design should be useful, meaningful, and should enrich our happiness and well-being.
                    </p>
                </div>
                <div class="main_button_holder">
                    <a class="main_button wow fadeInDown" data-wow-offset="200" href="">
                        Read our Story
                    </a>
                </div>
                <div class="bottom_line wow fadeIn"></div>
            </div>
        </div>
    </div>
</section>