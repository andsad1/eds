<?php get_header(); ?>
<div class="portfolio_slider wow fadeIn" data-wow-delay="0.3s" data-wow-duration="1.2s">

    <?php if (have_posts()) : while (have_posts()) :
            the_post(); ?>
            <?php $thumb = get_the_post_thumbnail_url(get_the_id(), 'big'); ?>
            <div class="portfolio_slide">
                <a href="<?php the_permalink(); ?>">
                    <div class="portfolio_img_holder">
                        <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>">
                        <div class="portfolio_text">
                            <h2><?php the_title(); ?></h2>
                            <h4><?php the_field('sub_title'); ?></h4>
                        </div>
                    </div>
                </a>
            </div>
    <?php
        endwhile;
    endif; ?>
</div>

<?php get_footer(); ?>