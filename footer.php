<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer_text text">
                    <h2>edit design studio</h2>
                    <p>Rossacoosane, Kenmare, Co. Kerry, Ireland</p>
                    <p>hello@editdesignstudio.ie</p>
                    <div class="footer_social">
                        <a href="">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a href="">
                            <i class="fab fa-pinterest"></i>
                        </a>
                        <a href="">
                            <i class="fab fa-houzz"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>

</html>