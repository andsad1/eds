<?php

require_once("includes/register_stylesheets.php");
require_once("includes/register_scripts.php");
require_once("includes/register_cpt.php");

add_image_size('big', 1600, 9999);
add_theme_support('post-thumbnails');

function register_my_menu(){
    register_nav_menus(
        array(
            'pagrindinis-meniu' => __('Pagrindinis meniu')
        )
    );
}
add_action( 'after_setup_theme', 'register_my_menu' );

//Remove wp version
function wpb_remove_version()
{
    return '';
}
add_filter('the_generator', 'wpb_remove_version');

//require_once(get_template_directory() . "/includes/register_acf_options.php");
//require_once("includes/pll_strings.php");
//require_once(get_template_directory() . "/includes/register_shortcodes.php");

//Google maps API
//function my_acf_google_map_api( $api ){
//
//    $api['key'] = 'AIzaSyCFWYCoNC_yZQOKAtdK4E4aHyjEXjqh89Y';
//
//    return $api;
//
//}
// add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

add_action('admin_menu', 'remove_default_post_type');

function remove_default_post_type()
{
    remove_menu_page('edit.php');
}
