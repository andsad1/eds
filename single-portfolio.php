<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <main id="single_portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-5 offset-lg-1">
                        <?php while (have_rows('gallery')) : the_row(); ?>
                            <?php $image = get_sub_field('image'); ?>
                            <?php if ($image) : ?>
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['title']; ?>">
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                    <div class="col-lg-5">
                        <h1 class="wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.3s"><?php the_title(); ?></h1>
                        <div class="text excerpt_holder wow fadeIn" data-wow-duration="1.6s" data-wow-delay="0.6s">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="text content_holder">
                            <?php the_content(); ?>
                        </div>
                        <div class="portfolio_permalink_outter">
                            <div class="portfolio_permalink_holder wow fadeInRight"  data-wow-duration="1.2s" data-wow-delay="0.2s">
                                <div class="read_more_line">
                                </div>
                                <a href="<?php the_permalink(); ?>" class="portfolio_read_more">
                                    Read More
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 other_posts">
                    <?php $next_post = get_next_post(); ?>
                    </div>
                </div>
            </div>
        </main>
<?php
    endwhile;
endif; ?>
<?php get_footer(); ?>