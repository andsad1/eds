"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require("gulp-autoprefixer");
const uglify = require("gulp-uglify");
const csso = require("gulp-csso");
const rename = require("gulp-rename");
const browserSync = require("browser-sync").create();


sass.compiler = require("node-sass");

gulp.task("sass", () => {
    return gulp
        .src("./assets/scss/**/*.scss")
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                outputStyle: "compressed"
            }).on("error", sass.logError)
        )
        .pipe(
            autoprefixer({
                browsers: ["last 6 versions"]
            })
        )
        .pipe(csso())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest("./assets/dist"))
        .pipe(browserSync.stream());
});

gulp.task("sass:watch", () => {
    gulp.watch("./assets/scss/**/*.scss", ["sass"]);
});

gulp.task("scripts", () => {
    return gulp
        .src("./assets/js/**/*.js")
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write("./maps"))
        .pipe(gulp.dest("./assets/dist"));
});

gulp.task("development", () => {
    browserSync.init({
        proxy: "localhost/eds"
    });
    gulp.watch("./**/*.php").on("change", browserSync.reload);
    gulp.watch("./assets/scss/**/*.scss", ["sass"]);
    gulp.watch("./assets/js/**/*.js", ['scripts']);
});

gulp.task("production", ["sass", "scripts"]);