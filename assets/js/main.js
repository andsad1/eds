jQuery(document).ready(function ($) {
  $('.hero_slider').slick({
    dots: false,
    infinite: true,
    speed: 600,
    autoplaySpeed: 3000,
    arrows: true,
    autoplay: true,
    fade: true,
    cssEase: 'linear'
  });
  $('.journal_slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    speed: 500,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
  });
  $('.portfolio_slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    speed: 500,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true
  });
  // Header Shrink
  $(document).scroll(function() {
    if ($(document).scrollTop() >= 100) {
      $('#desktop_header').addClass('scrolled_header');
    } else {
      $('#desktop_header').removeClass('scrolled_header');
    }
  });
  // sub menu toggle
  $('#desktop_header .menu-item-has-children').mouseenter(function(){
    $(this).find('.sub-menu').slideDown(300);
  });
  $('#desktop_header .menu-item-has-children').mouseleave(function(){
    $(this).find('.sub-menu').slideUp(300);
  }); 
  // Portfolio text toggle
  var check = true;
  $('.portfolio_read_more').click(function(e){
    e.preventDefault();
    $('.content_holder').toggle();
    $('.excerpt_holder').toggle();
    if (check) {
      $(this).text('Read Less');
      check = false;
    } else {
      $(this).text('Read More');
    }
  });
  // wow js
  new WOW().init();
});