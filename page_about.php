<?php /* Template Name: About us */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <main id="about_us">
            <?php $thumb = get_the_post_thumbnail_url(get_the_id(), 'big'); ?>
            <img class="main_img"  src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <h1 class="wow fadeInLeft" data-wow-offset="200" data-wow-delay="0.3s" data-wow-duration="1.2s"><?php the_title(); ?></h1>
                        <div class="text wow fadeIn" data-wow-offset="200" data-wow-delay="0.3s" data-wow-duration="1.2s">
                            <?php the_content(); ?>
                        </div>
                        <div class="bottom_line"></div>
                    </div>
                </div>
            </div>
        </main>
<?php
    endwhile;
endif; ?>
<?php get_footer(); ?>