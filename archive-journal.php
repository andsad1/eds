<?php get_header(); ?>
<main id="journal_archive">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 offset-lg-2">
                <?php if (have_posts()) : while (have_posts()) :
                        the_post(); ?>
                        <article class="one_journal wow fadeIn" data-wow-delay="0.3s" data-wow-duration="1.4s">
                            <?php $thumb = get_the_post_thumbnail_url(get_the_id(), 'large'); ?>
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>">
                            </a>
                            <div class="tagline_date">
                                <?php if (get_field('tagline')) : ?>
                                    <?php the_field('tagline'); ?> -
                                <?php endif; ?>
                                <?php the_time('j F Y'); ?>
                            </div>
                            <h1><?php the_title(); ?></h1>
                            <div class="text">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="journal_permalink_holder">
                                <div class="read_more_line">
                                </div>
                                <a href="<?php the_permalink(); ?>" class="journal_read_more">
                                    Read More
                                </a>
                            </div>

                        </article>
                <?php
                    endwhile;
                endif; ?>
                <div class="pagination_holder">
                <?php the_posts_pagination(array(
                    'mid_size' => 2, 'prev_text' => '<span></span>',
                    'next_text' => '<span></span>'
                )); ?>
                </div>
                <div class="bottom_line"></div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>