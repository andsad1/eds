<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <main id="contact">
            <?php $thumb = get_the_post_thumbnail_url(get_the_id(), 'big'); ?>
            <img class="main_img"  src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>">
            <div class="container main_contact_container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="contact_form_holder wow fadeIn" data-wow-duraion="1.2s" data-wow-delay="0.3s" data-wow-offset="200">
                            <form action="" id="contact_form">
                                <input type="text" placeholder="First Name">
                                <input type="text" placeholder="Last Name">
                                <input type="email" placeholder="E-mail">
                                <textarea name="message" id="" rows="2" placeholder="Message"></textarea>
                                <div class="contact_submit_holder">
                                    <div class="contact_submit_line">
                                    </div>
                                    <button id="contact_submit">
                                        Send
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="contact_holder wow fadeIn" data-wow-duraion="1.2s" data-wow-delay="0.5s" data-wow-offset="200">
                            <div class="one_contact">
                                <h2>Justina</h2>
                                <div class="contact_line">
                                    <div class="contact_line_name">
                                        Call
                                    </div>
                                    <a href="">+353 87 693 2497</a>
                                </div>
                                <div class="contact_line">
                                    <div class="contact_line_name">
                                        E-mail
                                    </div>
                                    <a href="">justina@editdesignstudio.ie</a>
                                </div>
                            </div>
                            <div class="one_contact">
                                <h2>Nathalie</h2>
                                <div class="contact_line">
                                    <div class="contact_line_name">
                                        Call
                                    </div>
                                    <a href="">+353 87 922 2290</a>
                                </div>
                                <div class="contact_line">
                                    <div class="contact_line_name">
                                        E-mail
                                    </div>
                                    <a href="">nathalie@editdesignstudio.ie</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact_adress">
                Roosacoosane, Kenmare, Co. Kerry, V93K5D7 Ireland
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="bottom_line"></div>
                    </div>
                </div>
            </div>
        </main>
<?php
    endwhile;
endif; ?>
<?php get_footer(); ?>