<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <main id="single_journal">
            <?php include ('includes/components/plain_slider.php'); ?>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <div class="tagline_date">
                            <?php if (get_field('tagline')) : ?>
                                <?php the_field('tagline'); ?>
                            <?php endif; ?>
                            <?php the_time('F j, Y'); ?>
                        </div>
                        <h1 class="wow fadeInLeft" data-wow-delay="0.3s" data-wow-duration="1.2s"><?php the_title(); ?></h1>
                        <div class="text">
                            <?php the_content(); ?>
                        </div>
                        <div class="bottom_line"></div>
                    </div>
                </div>
            </div>
        </main>
<?php endwhile;
endif; ?>
<?php get_footer(); ?>