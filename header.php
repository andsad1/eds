<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="author" content="Andrius Sadauskas">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <!--    Social sharing    -->
    <meta property="og:title" content="<?php is_front_page() ? bloginfo('name') : wp_title(); ?>">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png">
    <meta property="og:description" content="<?php bloginfo('description'); ?>">
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <header id="desktop_header">
        <div class="header_holder row">
            <div class="logo_holder">
                <a href="<?php echo get_home_url(); ?>">
                    <img class="logo" src="<?php bloginfo('template_url'); ?>/assets/img/logo.png" alt="editdesignstudio logo">
                </a>
            </div>
            <nav>
                <?php wp_nav_menu(array(
                    'menu' => 'Main Menu',
                )); ?>
            </nav>
            <div class="second_logo_holder">
                <a href="<?php echo get_home_url(); ?>">
                    <h3>edit design studio</h3>
                    <div class="second_logo_text">INTERIOR ARCHITECTURE <span></span> INTERIOR DESIGN</div>
                </a>
            </div>
        </div>

    </header>